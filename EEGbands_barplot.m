%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                      %
%   Author: Leandro Lecca Villacorta   %
%   Contact: lean.lecca.96@icloud.com  %
%                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% function to plot on a bar graph the analyzed EEG bands for each segment
% under study

function EEGbands_barplot(results, rec, patients, band_names, n_seg, ...
    segment_names, labelYax)

% Input: 
% 1. results: results to be analyzed (energy or power in this case).
% 2. rec: name of all recordings.
% 3. patients: patient's ID.
% 4. band_names: name of the EEG bands analyzed.
% 5. n_seg: number of segments analyzed in the recording.
% 6. segment_names: name of the segments analyzed in the recording.
% 7. labelYax: label to show on the Y axis (this will also be the title of
%              the figure).

dim_subplot = ceil(sqrt(length(patients)));

colors = zeros(size(n_seg,2),3);

for i=1:n_seg
    fprintf("Choose a color for sub-group bar.\n")
    colors(i,:) = uisetcolor;
end

fig = figure('Renderer', 'painters', 'Position', [10 10 1000 700]);

for i=1:length(patients)
    % find in the table all the results attached to the i-th patient
    aux_res = results((strncmp(rec,patients{i},5)));
    
    % compute the mean for all the results found
    if (size(aux_res,1) ~= 1)
        aux_res = mean(cat(size(aux_res,1), aux_res{:}),size(aux_res,1));
    else
        aux_res = cell2mat(aux_res);
    end
    
    % bar plot:
    subplot(dim_subplot, dim_subplot, i);
    b = bar(aux_res,'FaceColor','flat'); box off;
    
    for j=1:size(aux_res,2)
        b(j).CData = colors(j,:);
    end
    
    ylabel(labelYax);
    title(['Patient: ' patients{i}]);
    set(gca, 'XTickLabel', band_names,'FontSize',16);
    legend(segment_names, 'Box', 'off');
    
end

h = suptitle(labelYax); set(h, 'FontSize', 18, 'FontWeight', 'Bold');

saveas(fig, fullfile([pwd '/figures/'], labelYax), 'svg');

end