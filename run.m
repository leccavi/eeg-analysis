%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                      %
%   Author: Leandro Lecca Villacorta   %
%   Contact: lean.lecca.96@icloud.com  %
%                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% plotting some recording files:
plotEEG('chb05_13.edf')
plotEEG('chb08_05.edf', 2840, 3100)


% obtaining the name of the recordings:
rec = struct2cell(dir(fullfile([pwd '/data/'],'*.edf')));
FileName = cell(length(rec), 1);

% creating the table to store the energy and power results:
results = table('Size', [length(FileName) 3], 'VariableTypes', ...
    {'string','cell','cell'}, 'VariableNames', ...
    {'record', 'energy', 'power'});


% computing the energy and power results for the recordings:
for i=1:length(rec)
    [~, FileName{i}, ~] = fileparts(fullfile([pwd '/data/'], rec{1,i}));
    [energy, power] = anictal(FileName{i});
    results.energy(i) = mat2cell(energy, size(energy,1), size(energy,2));
    results.power(i) = mat2cell(power, size(power,1), size(power,2));
end

results.record = FileName;

% information for the representation part:
patients = {'chb05'; 'chb08'; 'chb09'; 'chb10'};
band_names = {'Delta', 'Theta', 'Alpha', 'Beta', 'Gamma'};
segment_names = {'Pre-ictal','Ictal','Post-ictal'};

% results representation:

% energy:
EEGbands_barplot(results.energy, results.record, patients, band_names, ...
    length(segment_names), segment_names, 'Normalized energy');

% power:
EEGbands_barplot(results.power, results.record, patients, band_names, ...
    length(segment_names), segment_names, 'Power');