# Analysis of electroencephalogram (EEG) signals

The aim of this project is to characterize the changes in EEG signals during the epileptic seizure (ictal period). For this purpose, this period is compared with the parameters obtained for the pre-ictal and post-ictal periods. These parameters are the normalized energy and power for five frequency bands (Delta, Theta, Alpha, Beta and Gamma).

The data come from the [CHB-MIT Scalp EEG](https://physionet.org/content/chbmit/1.0.0/) database available at Physionet. The start (_ictal-onset_) and end (_ictal-offset_) of the epileptic seizure was determined by experts, while the pre-ictal period is the 60-second period before the _ictal-onset_ and the post-ictal period is the 120-second period after the _ictal-offset_. The segmentation of these periods is in files with extension _.txt_.

[anictal.m](https://bitbucket.org/leccavi/eeg-analysis/src/master/anictal.m), [plotEEG.m](https://bitbucket.org/leccavi/eeg-analysis/src/master/plotEEG.m), [EEGbands_barplot.m](https://bitbucket.org/leccavi/eeg-analysis/src/master/EEGbands_barplot.m) have been fully programmed, while [edfread.m](https://bitbucket.org/leccavi/eeg-analysis/src/master/edfread.m) was provided to read the file format of the recordings. Within [anictal.m](https://bitbucket.org/leccavi/eeg-analysis/src/master/anictal.m) you can find more information in the comments to understand how the parameters are obtained.

Execute the entire project by running the [run.m](https://bitbucket.org/leccavi/eeg-analysis/src/master/run.m) file.

Some results:

- EEG visualization:

![picture](figures/Record%20ID:%20chb05-13.svg)

- Summary of results:

![picture](figures/Normalized%20energy.svg)

![picture](figures/Power.svg)