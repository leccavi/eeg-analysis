%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                      %
%   Author: Leandro Lecca Villacorta   %
%   Contact: lean.lecca.96@icloud.com  %
%                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [energy, power] = anictal(rec)

% Input: 
% 1. rec: name of the recording without the file extension
% Output:
% 1. Energy: matrix containing the computed normalized energy by freq-bands 
%            (delta, theta, alpha, beta and gamma) (rows) for each segment 
%            (columns). Note that the normalized energy of each segment is 
%            the average of the normalized energies of each electrode for 
%            that segment. This has be done in order to not generate a huge
%            amount of results.
% 2. Power: matrix containing the computed power by freq-bands (delta, 
%           theta, alpha, beta and gamma) (rows) for each segment 
%           (columns). Note that the power of each segment is the average
%           of the power of each electrode for that segment.



[~, data] = edfread([pwd '/data/' rec '.edf']);

% reading segments:
PIctal = importdata([pwd '/data/' rec '_seg.txt']);

% filtering step:
fm = 256; % sample frequency
fci = 59; % lower frequency
fcs = 61; % higher frequency
fnorm = fm/2; % Nyquist normalization

% pulses
wi = fci/fnorm; 
ws = fcs/fnorm; 
wn = [wi ws];

% band-stop filter (to reduce grid interference) of 5th order:
[A,B] = butter(5, wn, 'stop'); 

recordfilt = filtfilt(A,B,data'); % filters along the 1st dimension, that
                                  % is why data must be trasposed

% Spectral analysis of each EEG channel

% Hamming window:
window = hamming(20*fm);

% overlap between windows in samples:
noverlap = 10*fm;

% number of points on which the power spectral density is calculated:
nfft = 4096;

% segments of the signal
segments = PIctal.data*fm;
[nseg,~] = size(segments);

% frequency definition:
df = fm/nfft;

% frequency bands:
% Delta: 1-4 Hz
% Theta: 4-8 Hz
% Alpha: 8-12 Hz
% Beta: 12-30 Hz
% Gamma: 30-128 Hz

f1 = round(1/df)+1; 
f4 = round(4/df)+1; 
f8 = round(8/df)+1;
f12 = round(12/df)+1;   
f30 = round(30/df)+1; 
f128 = round(128/df)+1;

% Definition of energy variables for each segment by freq-bands
E1 = zeros(1,nseg); E2 = E1; E3 = E1; E4 = E1; E5 = E1;

% Idem for the power:
P1 = zeros(1,nseg); P2 = P1; P3 = P1; P4 = P1; P5 = P1;

for i=1:nseg
    ictal = recordfilt(segments(i,1):segments(i,2),:);

    [P,~] = pwelch(ictal,window,noverlap,nfft,fm);

    % normalized energy by freq-bands:
    NE_Delta = sum(P(f1:f4,:))./sum(P(f1:f128,:));
    NE_Theta = sum(P(f4:f8,:))./sum(P(f1:f128,:));
    NE_Alfa = sum(P(f8:f12,:))./sum(P(f1:f128,:));
    NE_Beta = sum(P(f12:f30,:))./sum(P(f1:f128,:));
    NE_Gamma = sum(P(f30:f128,:))./sum(P(f1:f128,:));

    % power by sub-bands:
    POT_Delta = sum(P(f1:f4,:)*df);
    POT_Theta = sum(P(f4:f8,:)*df);
    POT_Alfa = sum(P(f8:f12,:)*df);
    POT_Beta = sum(P(f12:f30,:)*df);
    POT_Gamma = sum(P(f30:f128,:)*df);
    
    % mean of the channels both for energy and power:
    E1(i)=mean(NE_Delta);
    E2(i)=mean(NE_Theta);
    E3(i)=mean(NE_Alfa);
    E4(i)=mean(NE_Beta);
    E5(i)=mean(NE_Gamma);

    P1(i)=mean(POT_Delta);
    P2(i)=mean(POT_Theta);
    P3(i)=mean(POT_Alfa);
    P4(i)=mean(POT_Beta);
    P5(i)=mean(POT_Gamma);
end

energy = [E1; E2; E3; E4; E5];
power = [P1; P2; P3; P4; P5];

end