%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                      %
%   Author: Leandro Lecca Villacorta   %
%   Contact: lean.lecca.96@icloud.com  %
%                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% function to plot EEGs channels

function plotEEG(rec, start, finish)

fm = 256;

[headers, data] = edfread([pwd '/data/' rec]);

rec(find(rec == '.',1,'last'):end) = [];
rec(find(rec == '_',1,'last')) = '-';

if nargin==1
    ttl = ['Record ID: ' rec];
    start=1;
    finish=size(data,2);
end

if nargin==2
    ttl = ['Record ID: ' rec ', from ' num2str(start) ...
        ' to end s.'];
    start=start*fm+1;
    finish=size(data,2);
end

if nargin==3
    ttl = ['Record ID: ' rec ', from ' num2str(start) ...
        ' to ' num2str(finish) ' [s]'];
    start=start*fm+1; 
    finish=finish*fm;
end

time=1/fm:1/fm:size(data,2)/fm;
close all;

inter=0;
n=size(data,1);

bottom=0.05;
Height=(1-bottom-0.06-inter*(n-1))/n;

figure('Renderer', 'painters', 'Position', [10 10 1000 800]); 

for i=1:n
    subplot('position',[0.05 bottom 0.88 Height]); 
    plot(time(start:finish),data(i,start:finish),'k');
    set(gca, 'YLim', [-1000, 1000]);
    set(gca, 'XLim', [time(start) time(finish)]);
    bottom=bottom+inter+Height; 
    set(gca,'Color','none');
    set(gca,'box','off');
    set(gca,'XColor',[0.831 0.816 0.784])
    set(gca,'XTick',[]);
    set(gca,'YColor',[0.831 0.816 0.784])    
    set(gca,'YTick',[]);
    ylabel(headers.label{i},'FontSize',11,'FontWeight','bold','Color','b');
    set(get(gca,'ylabel'),'rotation',0, 'VerticalAlignment','middle', ...
        'HorizontalAlignment','right');
end

fig = gcf;

% Set xlabel at the bottom subplot for the whole figure:
fig.Children(n).XLabel.Color = 'b';
fig.Children(n).XLabel.FontSize = 16;
fig.Children(n).XLabel.String = 'Time [s]';
fig.Children(n).XLabel.FontWeight = 'bold';

% Set title at the first subplot for the whole figure:
pos = fig.Children(1).Position; pos(2) = pos(2)+0.06;
axes('Position', pos,'XAxisLocation','top');
set(gca, 'Color', 'None', 'XColor', 'None', 'YColor', 'None');
text(0.5, 0, ttl, 'FontSize', 14', ...
    'FontWeight', 'Bold', 'HorizontalAlignment', 'Center', ...
    'VerticalAlignment', 'Bottom', 'Color', 'Blue');

% Saving figure:
saveas(fig,fullfile([pwd '/figures/'], ttl),'svg');